<?php
/**
 * this is the index file which should be used as the initiator for all calls in the system
 * @author Christopher Bartolo <chris@chrisbartolo.com>
 **/
namespace Blexr;
use Blexr\Lib\Bootstrap;
use Blexr\Lib\Registry;

// set to show all errors and warning in the browser
error_reporting(E_ALL && ~E_NOTICE);
ini_set('display_errors', '1');
// start the session
session_start();


$documentRoot = dirname(__DIR__) . DIRECTORY_SEPARATOR;

// let the composer will handle autoloading
$autoloadFile = $documentRoot . 'vendor/autoload.php';

if (is_readable($autoloadFile)) {
    require_once  $autoloadFile;
}


$pathConfigFile =  $documentRoot . "Config/PathsConfig.php";

if (is_readable($pathConfigFile)) {
    require_once $pathConfigFile;
}


$Registry = new Registry();
//$Registry->Template = new Template($Registry);
$Bootstrap = new Bootstrap($Registry);

$Bootstrap->parseURL();
