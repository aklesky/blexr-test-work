<?php

/**
 * this is the error controller for the system; here we handle how errors are shown, handled etc.
 * @author Christopher Bartolo <chris@chrisbartolo.com>
 **/

namespace Blexr\Controller;

use Blexr\Base\ControllerBase;

class ErrorController extends ControllerBase
{
    public function __construct($registry)
    {
        parent::__construct($registry);
    }

    public function index()
    {

    }
}
