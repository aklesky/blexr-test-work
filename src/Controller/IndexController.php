<?php

/**
 * this is the index controller for the system; here we handle the first initial call to the system,
 * with blank parameters
 * @author Christopher Bartolo <chris@chrisbartolo.com>
 **/

namespace Blexr\Controller;

use Blexr\Base\ControllerBase;

class IndexController extends ControllerBase
{
    public function __construct($registry)
    {
        parent::__construct($registry);
    }

    public function index()
    {
        echo "Called Index Index";
    }
}
