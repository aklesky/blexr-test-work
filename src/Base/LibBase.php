<?php

/**
 * this is the base lib
 * @author Christopher Bartolo <chris@chrisbartolo.com>
 **/

namespace Blexr\Base;


use Blexr\Base;

abstract class LibBase extends Base
{

    /**
     * always initialise the base lib with the registry object which holds all values
     * LibBase constructor.
     * @param \Blexr\Lib\Registry $registry
     */
    public function __construct($registry)
    {
        parent::__construct($registry);
    }

}
