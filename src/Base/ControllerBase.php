<?php

/**
 * this is the base controller
 * @author Christopher Bartolo <chris@chrisbartolo.com>
 **/

namespace Blexr\Base;


use Blexr\Base;

abstract class ControllerBase extends Base
{
    /**
     * always initialise the base controller with the registry object which holds all values
     * ControllerBase constructor.
     * @param \Blexr\Lib\Registry $registry
     */
    public function __construct($registry)
    {
        parent::__construct($registry);
    }

    /*
     * a controller must always have an index function which is called by default
     */
    public abstract function index();
}
