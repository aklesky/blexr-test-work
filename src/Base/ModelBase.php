<?php

/**
 * this is the base controller
 * @author Christopher Bartolo <chris@chrisbartolo.com>
 **/

namespace Blexr\Base;


use Blexr\Base;

abstract class ModelBase extends Base
{
    /**
     * always initialise the base model class with the registry dependency which holds all values
     * ModelBase constructor.
     * @param \Blexr\Lib\Registry $registry
     */
    public function __construct($registry)
    {
        parent::__construct($registry);
    }

    /**
     * the default save function. All Models should have the ability to save details to the database
     * @param $args array
     */
    public abstract function save($args);

    /**
     * the default delete function. All Models should have the ability to delete details from the database
     * @param $args array
     */
    public abstract function delete($args);

}
