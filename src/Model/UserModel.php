<?php

/**
 * this is the model for the user creation and control for database functions
 * @author Christopher Bartolo <chris@chrisbartolo.com>
 **/

namespace Blexr\Model;

use Blexr\Base\ModelBase;

class UserModel extends ModelBase
{
    public function __construct($registry)
    {
        parent::__construct($registry);
    }

    /**
     * @param array $args
     */
    public function save($args)
    {

    }

    /**
     * @param array $args
     */
    public function delete($args)
    {

    }

    /**
     * @param $user_id
     * @param $passwordMd5
     */
    public function setPassword($user_id, $passwordMd5)
    {

    }

}
