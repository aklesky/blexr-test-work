<?php
/**
 * Base.php
 *
 * Author: Aleksey Semiglasov aleksey.semiglasov@gmail.com
 * Copyright Aleksey Semiglasov 14/05/2016
 */

namespace Blexr;


use Blexr\Lib\Registry;

abstract class Base
{
    /**
     * @var Registry
     */
    protected $registry = null;

    /**
     * always initialise the base class with the registry dependency which holds all values
     * Base constructor.
     * @param Registry $registry
     */

    public function __construct(Registry $registry)
    {
        $this->registry = $registry;
    }
}
