<?php

/**
 * this is the base rgistry which stores all vars and objects needed across all calls
 * @author Christopher Bartolo <chris@chrisbartolo.com>
 **/

namespace Blexr\Lib;


class Registry
{

    protected $vars = array();

    public function __get($index)
    {
        if (isset($this->vars[$index])) {
            return $this->vars[$index];
        } else {
            return false;
        }
    }

    public function __set($index, $value)
    {
        $this->vars[$index] = $value;
    }

    public function returnVariables()
    {
        return $this->vars;
    }
}
