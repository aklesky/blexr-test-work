# Blexr Test Work

List of changes:

* The Directory structure was changed
* The Namespaces were added
* The Base Class was added
* The code duplication was removed

 ```
      
      // before
      
     /*
      * The registry object
      */
      
     private $registry = null;
 
     /*
      * always initialise the base controller with the registry object wich holds all values
      */
      
     public function __construct($Registry)
     {
        $this->registry = $Registry;
     }
     ....
     
     // after
     
     public function __construct($registry)
     {
        parent::__construct($registry);
     }
     
 ```
 
 * The coding style was changed to PSR-2
 * index.php was renamed to bootstrap.php
 * Public directory now is the document root 
 * bootstrap.php file was fixed and refactored
 * __autoload function was replaced to the composer autoload
